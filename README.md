# README #

Instrucciones para prueba del desarrollo y requerimientos

### Requerimientos ###

* PHP 7.1 o superior 
* Composer

### Librerias utilizadas ###

* Laravel 5.6
* Guzzle 6.3
* Angular JS 1.6.9
* Bootstrap 4.0
* jQuery 3.3.1
* Eonasdan Bootstrap Datetimepicker 4.17.47
* Moment 2.22.0
* Font Awesome 5.0.9

### Instrucciones ###

* Clonar el repositorio en una carpeta del web server
* Obtener las librerias PHP mediante composer install
* Ejecutar dentro de la carpeta el comando php artisan serve, para correr el servidor web en el puerto 8000 del localhost
* Entrar a la URL http://localhost:8000 y realizar prueba de la aplicación

### API ###

* POST --> generateSession/ --> Genera el ID de Session
* GET --> stations/ --> Obtiene las estaciones solicitadas
* GET --> cars/ --> Obtiene los autos disponibles con el itinerario enviado