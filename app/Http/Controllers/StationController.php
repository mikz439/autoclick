<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Station;
use Validator;
use App\FaultMap;

class StationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $station = new Station();

        //Si en la peticion no se cumplen los formatos y campos requeridos, devolver error 400
        $validator = Validator::make($request->all(), $station::$rules);
        if ($validator->fails())
        {
            return response()->json($validator->errors(), 400);
        }

        //Realizar petición y convertir a JSON para su respuesta
        $response = json_decode($station->get($request->all()), true);

        //En caso de existir faultstrin en la respuesta del Web Server, devolver mensaje con error, else devolver respuesta
        if(isset($response['faultstring'])){
            $errors = FaultMap::faultJsonResponse($response['faultstring']);
            return response()->json($errors['msg'], $errors['code']);
        }else{
            return $response;
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
