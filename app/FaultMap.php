<?php

namespace App;

class FaultMap
{

    /* Funcion para devolver codigos HTTP en caso de recibir cierto string de respuesta del server
        ya que por ahora solo devuelve 200 OK */
    public static function faultJsonResponse($faultstring){

        $response = [
            'msg' => ['message' => 'Default MSG'],
            'code' => 400
        ];

        switch ($faultstring) {
            case "SessionId invalida.":
                $response['msg']['message'] = 'Hubo un problema al validar la sesión, por favor recargue la página.';
                $response['code'] = 401;
                break;
            case "Funcion no definida.":
                $response['msg']['message'] = 'Hubo un error en el servidor, por favor intente más tarde.';
                $response['code'] = 400;
                break;
            default:
                $response['msg']['message'] = $faultstring;
                $response['code'] = 400;
        }

        return $response;
    }
}