<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use GuzzleHttp\Client as Guzzle;

class SessionKey extends Model
{
    public $url = 'http://www.triyolo.com/ejercicio/rest/';
    public $response;

    //Reglas de validación
    public static $rules = [
        'ContractId'    => 'required|string',
        'Password'      => 'required|string',
        'LanguageId'    => 'required|string'
    ];

    //Se construye el cliente que consultara la API del Web Service en JSON
    public function __construct()
    {
        parent::__construct();
        $this->response = new Guzzle([
            'base_uri' => $this->url,
            'headers' => [
                'Content-Type' => 'application/json'
            ]
        ]);
    }

    //Se genera solicitud deseada con lo parametros enviados
    public function generate($params){
        $params['Function'] = 'LogIn';

        return $this->response->request('POST','', [
                    'body' => json_encode($params)
                ])->getBody();
    }
}
