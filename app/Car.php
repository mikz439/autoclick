<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use GuzzleHttp\Client as Guzzle;

class Car extends Model
{
    public $url = 'http://www.triyolo.com/ejercicio/rest/';
    public $response;

    //Reglas de validación
    public static $rules = [
        "SessionId"         =>  "required|string",
        "CheckOutStationId" =>  "required|string",
        "CheckOutDate"      =>  "required|date",
        "CheckInStationId"  =>  "required|string",
        "CheckInDate"       =>  "required|date"
    ];

    public function __construct()
    {
        parent::__construct();
        $this->response = new Guzzle([
            'base_uri' => $this->url,
            'headers' => [
                'Content-Type' => 'application/json'
            ]
        ]);
    }
    public function get($params){
        $params["Function"] = "GetCarAvailability";


        return $this->response->request('POST','', [
            'body' => json_encode($params)
        ])->getBody();
    }
}
