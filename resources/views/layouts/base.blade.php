<!DOCTYPE html>
<html lang="es" ng-app="autoclick">
<head>
    <meta charset="UTF-8">
    <title>Autoclick Car</title>

    <!-- Responsive Design -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Bower Components -->
    @stack('bowercss')
    <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css">
    <link rel="stylesheet" href="bower_components/font-awesome5/web-fonts-with-css/css/fontawesome-all.min.css">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="css/autoclick.css">

</head>
<body class="container-fluid" id="rootAutoclick" ng-controller="RootController as rt">

@yield('section')

<!-- Bower Components -->

@stack('bowerjs')

@stack('autoclickjs')


</body>
</html>