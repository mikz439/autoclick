@extends('layouts.base')

@push('bowercss')
<link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
<link rel="stylesheet" href="bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker-standalone.css">
<link rel="stylesheet" href="bower_components/font-awesome5/web-fonts-with-css/css/fontawesome-all.min.css">
@endpush

@section('section')

<!-- NAV HEADER -->
<nav id="navAuto" class="row navbar navbar-expand-xm navbar-light bg-light px-4 py-1">

    <a href="#" class="nav-link">
        <i class="fa fa-align-justify gray-click-text">
        </i>
    </a>

    <a class="navbar-brand ml-4" href="#">
        <img src="images/logotipo_autoclick.png" alt="AutoClick Rent a Car" width="60px">
    </a>

    <a class="ml-md-auto nav-link gray-click-text" href="#">
        <i class="fa fa-phone">

        </i>
    </a>

</nav>
<!-- ./NAV HEADER -->

@verbatim

<!-- COTIZADOR CONTROLLER -->
<section id="cotizador" ng-controller="CotizadorController as vm">

    <!-- FORM ITINERARIO -->
    <div class="container py-4">
        <div class="row">
            <div class="col-md-12 m-auto">
            <div ng-show="vm.sections.form" class="form-container col-md-12 azul-click mb-3 fade-in-out">
                <div class="row">

                    <div class="ribbon">
                        <h2><i>Elige fecha y lugar</i></h2>
                    </div>

                    <div class="col-md-12">
                        <h1 class="h1-step-1 float-right">Paso 1</h1>
                    </div>
                    <form class="my-3" name="dateForm" ng-submit="vm.verificarDisponibilidad()" id="dateForm" >
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-container">
                                        <label>
                                            <strong>OFICINA DE ENTREGA</strong>
                                            <small ng-show="vm.spinner.estaciones_in"><i class="fa fa-spin fa-spinner"></i> Cargando</small>
                                        </label>
                                        <select
                                                name="CheckInStationId"
                                                ng-model="vm.carParams.CheckInStationId"
                                                class="custom-select"
                                                ng-options="est.StationId as est.StationName for est in vm.estaciones_in"
                                                ng-disabled="vm.spinner.estaciones_in"
                                                required>
                                            <option value=""></option>
                                        </select>
                                        <span class="invalid-msg" ng-show="dateForm.CheckInStationId.$touched && dateForm.CheckInStationId.$error.required" ng-cloak>Se requiere elegir una oficina de entrega.</span>
                                    </div>
                                    <div class="row my-4">
                                        <div class="col-6">
                                            <div class="form-container">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <div class="input-group-text date-input-icon"><i class="fa fa-calendar"></i></div>
                                                    </div>
                                                    <input id="date_in" name="CheckInDate" ng-model="vm.carParams.CheckInDate" class="date-input form-control" placeholder="01/08/2018" readonly required />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="form-container">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <div class="input-group-text date-input-icon"><i class="fa fa-clock"></i></div>
                                                    </div>
                                                    <input name="CheckInTime" id="time_in" ng-model="vm.carParams.CheckInTime" class="date-input form-control" placeholder="10:00" readonly required />
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-md-6">
                                    <div class="form-container">
                                        <label>
                                            <strong>OFICINA DE DEVOLUCION</strong>
                                            <small ng-show="vm.spinner.estaciones_out"><i class="fa fa-spin fa-spinner"></i> Cargando</small>
                                        </label>
                                        <select
                                                name="CheckOutStationId"
                                                ng-model="vm.carParams.CheckOutStationId"
                                                class="custom-select"
                                                ng-options="est.StationId as est.StationName for est in vm.estaciones_out"
                                                ng-disabled="vm.spinner.estaciones_out"
                                                required>
                                            <option value=""></option>
                                        </select>
                                        <span class="invalid-msg" ng-show="dateForm.CheckOutStationId.$touched && dateForm.CheckOutStationId.$error.required" ng-cloak>Se requiere elegir una oficina de devolución.</span>
                                    </div>
                                    <div class="row my-4">
                                        <div class="col-6">
                                            <div class="form-container">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <div class="input-group-text date-input-icon"><i class="fa fa-calendar"></i></div>
                                                    </div>
                                                    <input type="text" id="date_out" name="CheckOutDate" ng-model="vm.carParams.CheckOutDate" class="date-input form-control" placeholder="01/08/2018" readonly required />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="form-container">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <div class="input-group-text date-input-icon"><i class="fa fa-clock"></i></div>
                                                    </div>
                                                    <input type="text" name="CheckOutTime" id="time_out" ng-model="vm.carParams.CheckOutTime" class="date-input form-control" placeholder="10:00" readonly required />
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 px-2">
                            <div class="col-md-12  white"></div>
                        </div>
                        <div class="d-flex col-md-12 justify-content-between py-4 align-items-center">
                            <a class="link-hr" href="#"><strong><u>OFICINAS PRINCIPALES</u></strong></a>
                            <button class="btn btn-info btn-lg azul-gradiente" type="submit">Continuar</button>
                        </div>
                        <div ng-show="vm.alertDate.status" class="col-12 text-right" ng-cloak>
                            <span class="invalid-msg">Falta definir las fechas y horas</span>
                        </div>
                        <div class="col-12 text-right" ng-show="vm.spinner.carros" ng-cloak>
                            <i class="fa fa-spin fa-spinner"></i> Buscando automóviles
                        </div>
                    </form>
                </div>
            </div>

        </div>

        </div>

    </div>
    <!-- ./FORM ITINERARIO -->

    <!-- RIBBON GRAY -->
    <div ng-show="vm.sections.form" class="row p-2 my-5 gray-rinbbon d-flex justify-content-center">
        <i><h2>Renta desde <span>$30</span> al día</h2></i>
    </div>
    <!-- ./RIBBON GRAY -->

    <!-- DISPLAY CARROS -->
    <div id="display" class="container my-2 fade-in-out" ng-show="vm.sections.disp" ng-cloak>

        <!-- DETALLES ITINERARIO -->
        <div class="step-2 col-md-12 m-auto">
            <div class="row d-flex justify-content-center">
                <div class="col-4 col-xs-4 step-pill step-pill-1">
                    <h2>Paso 1 <small>Su itinerario</small></h2>
                </div>
                <div class="col-4 step-pill step-pill-2">
                    <h2>Paso 2 <small>Seleccione su Auto</small></h2>
                </div>
                <div class="col-4 col-sm-4 step-pill step-pill-3">
                    <h2>Paso 3 <small>Extra-Reserva</small></h2>
                </div>
            </div>
            <div class="row py-2">
                <div class="col-md-6">
                    <h4 class="azul-click-text">Lugar y Fecha de Entrega:</h4>
                    <p ng-cloak>{{vm.detalleItinerario.CheckInStationName}}
                        <br> {{vm.formatoFecha(vm.detalleItinerario.CheckInDate, 'YYYY-MM-DDTHH:mm:ss.SSSZ', 'dddd DD MMMM YYYY - hh:mm a')}}</p>
                </div>
                <div class="col-md-6">
                    <h4 class="azul-click-text">Lugar y Fecha de Entrega:</h4>
                    <p ng-cloak>{{vm.detalleItinerario.CheckOutStationName}}
                        <br> {{vm.formatoFecha(vm.detalleItinerario.CheckOutDate, 'YYYY-MM-DDTHH:mm:ss.SSSZ', 'dddd DD MMMM YYYY - hh:mm a')}}</p>
                </div>
            </div>
            <div class="row d-flex justify-content-center py-2">
                <button ng-click="vm.modificarItinerario()" class="btn btn-info azul-gradiente">Modificar Itinerario</button>
            </div>

        </div>
        <!-- ./DETALLES ITINERARIO -->

        <!-- DETALLES CARROS -->
        <div class="col-md-12">
            <h2 class="mt-4 azul-click-text">Seleccione su Auto</h2>
            <h3 class="mb-4 azul-click-text"><i><small><strong>CATEGORIAS DE AUTOS:</strong></small></i></h3>

            <!-- PILLS CATEGORIAS -->
            <ul id="car-pills" class="mb-4 car-pills nav nav-pills nav-fill">
                <li class="nav-item">
                    <a class="nav-link active" href="javascript:void(0)">Todas <span class="circle"></span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="javascript:void(0)">Económico <span class="circle"></span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="javascript:void(0)">Compactos <span class="circle"></span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="javascript:void(0)">Intermedios <span class="circle"></span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="javascript:void(0)">Familiares <span class="circle"></span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="javascript:void(0)">SW <span class="circle"></span></a>
                </li>
            </ul>
            <!-- ./PILLS CATEGORIAS -->

            <!-- ELEGIR CARRO -->
            <div ng-show="vm.carros.length > 0" class="row">
                <div class="col-md-12 my-2 fade-in-out" ng-repeat="carro in vm.carros">
                    <div class="row">
                        <div class="col-md-6">
                            <img ng-src="{{'images/car_' + carro.Id + '.png'}}" width="100%" alt="">
                        </div>
                        <div class="col-md-6">
                            <h4 class="gray-click-text">{{carro.Group.CarModel}}</h4>
                            <h5 class="gray-click-text"><span class="azul-click-text">{{carro.Group.GroupName}}</span> ({{carro.Group.SippCode}})</h5>

                            <div class="row my-4">
                                <div class="feat col-3">
                                    <span class="aut aut-pax"></span>
                                    <span ng-cloak>{{carro.Features.Pax}}</span>
                                </div>

                                <div class="feat col-3">
                                    <span class="aut aut-door"></span>
                                    <span ng-cloak>{{carro.Features.Doors}}</span>
                                </div>

                                <div class=" feat col-3">
                                    <span class="aut aut-lug"></span>
                                    <span ng-cloak>{{carro.Features.BigLuggage}}</span>
                                </div>

                                <div class="feat col-3">
                                    <span ng-show="carro.Features.AirCondition" class="aut aut-ac"></span>
                                </div>
                            </div>

                            <div class="row my-4">
                                <div class="col-6">
                                    <div ng-show="carro.Features.Automatic" class="feat">
                                        <span class="aut aut-trans-auto"></span>
                                        <span>Automático</span>
                                    </div>
                                    <div ng-show="!carro.Features.Automatic" class="feat">
                                        <span class="aut aut-trans-manual"></span>
                                        <span>Manual</span>
                                    </div>
                                </div>
                                <div class="col-6 text-right">
                                    <p class="gray-click-text">Desde:</p>
                                    <h4 class="azul-click-text"><strong><i>{{carro.CarValuation.Total | currency}}</i></strong></h4>
                                </div>
                            </div>
                            <div class="row my-4">
                                <div class="col-6">
                                    <p class="azul-click-text"><u><strong>*Precio por día</strong></u></p>
                                </div>
                                <div class="col-6 d-flex justify-content-end">
                                    <button class="btn btn-info azul-gradiente">Reservar</button>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <!-- ELEGIR CARRO -->

        </div>
        <!-- ./DETALLES CARROS -->

    </div>
    <!-- DISPLAY CARROS -->

</section>
<!-- COTIZADOR CONTROLLER -->

@endverbatim


<!-- FOOTER FIXED -->
<footer class="row">
    <div class="footer-auto col-md-12 azul-gradiente p-3">
        <div class="row d-flex align-content-center">
            <div class="col-lg-2 col-md-3 col-4">
                <img src="images/logotipo_autoclick_white.png" alt="AutoClick Rent a Car" width="100%">
            </div>
            <div class="left-line col-lg-10 col-md-9 col-8">
                <p>Atención al cliente <br>
                    01 800 272 02 91 <br>
                    Nuestro horario de atención telefónica es 24 hrs.</p>
            </div>
        </div>
    </div>

    <div class="footer-bottom col-md-12 py-1">
        <div class="row">
            <div class="col-6 text-center">
                <a href="#">Sitio clásico</a>
            </div>
            <div class="col-6 text-center">
                <a href="#">Términos y condiciones</a>
            </div>
        </div>
    </div>
</footer>
<!-- ./FOOTER FIXED -->

@endsection

@push('bowerjs')
<script src="bower_components/jquery/dist/jquery.min.js" type="application/javascript"></script>
<script src="bower_components/angular/angular.min.js" type="application/javascript"></script>
<script src="bower_components/angular-animate/angular-animate.min.js" type="application/javascript"></script>
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js" type="application/javascript"></script>
<script src="bower_components/moment/min/moment.min.js"></script>
<script src="bower_components/moment/locale/es.js"></script>
<script src="bower_components/eonasdan-bootstrap-datetimepicker/src/js/bootstrap-datetimepicker.js" type="application/javascript"></script>
@endpush

@push('autoclickjs')
<script src="autoclick/cotizador/cotizador.datetime.js" type="application/javascript"></script>
<script src="autoclick/cotizador/cotizador.script.js" type="application/javascript"></script>

<script src="autoclick/app.module.js" type="application/javascript"></script>
<script src="autoclick/keepalive.factory.js" type="application/javascript"></script>
<script src="autoclick/carros/carro.factory.js" type="application/javascript"></script>
<script src="autoclick/estaciones/estacion.factory.js" type="application/javascript"></script>

<script src="autoclick/root.controller.js" type="application/javascript"></script>
<script src="autoclick/cotizador/cotizador.controller.js" type="application/javascript"></script>
@endpush