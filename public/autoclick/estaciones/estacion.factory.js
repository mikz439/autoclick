(function(){
    angular
        .module('autoclick')
        .factory('Estacion', Estacion);

    Estacion.$inject = ['$http', '$q'];

    function Estacion($http, $q) {

        var urlBase = '/api/stations';

        return {
            getAll: getAll
        };

        function getAll(e) {
            return $http.get(urlBase, {params: e})
                .then(function(response){
                        return response;
                    },
                    function(error){
                        throw error;
                    })
                .catch(function(error){
                    return $q.reject(error);
                });
        };
    }
})();
