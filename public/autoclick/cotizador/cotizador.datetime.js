$(document).ready(function () {

    /* Uso del plugin de datetime para fecha y hora */
    $('#date_in').datetimepicker({
        locale: 'es',
        format: 'DD/MM/YYYY',
        minDate: moment().millisecond(0).second(0).minute(0).hour(0),
        ignoreReadonly: true,
    }).on('dp.change', function(e){
        $(this).trigger('input');
        $('#date_out').data('DateTimePicker').minDate(e.date);
    });

    $('#time_in').datetimepicker({
        locale: 'es',
        format: 'HH:mm',
        ignoreReadonly: true,
    }).on('dp.change', function(e){
        $(this).trigger('input');
    });

    $('#date_out').datetimepicker({
        locale: 'es',
        format: 'DD/MM/YYYY',
        minDate: moment().millisecond(0).second(0).minute(0).hour(0),
        ignoreReadonly: true,
    }).on('dp.change', function(e){
        $(this).trigger('input');
    });

    $('#time_out').datetimepicker({
        locale: 'es',
        format: 'HH:mm',
        ignoreReadonly: true,
    }).on('dp.change', function(e){
        $(this).trigger('input');
    });

});