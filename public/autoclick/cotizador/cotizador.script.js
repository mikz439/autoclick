$(document).ready(function(){

    /* Efecto de circulo en los nav-links para agregar clase active a la presionada */
    $('#cotizador #display .car-pills .nav-link').on('click', function(){
        $(this).parent().siblings().children('.nav-link').removeClass('active');
        $(this).addClass('active');
    });

});