angular
    .module('autoclick')
    .controller('CotizadorController', CotizadorController);

CotizadorController.$inject = ['Estacion', 'Carro'];

function CotizadorController(Estacion, Carro) {
    var vm = this;

    /* Objects */
    vm.estaciones_in    = {};
    vm.estaciones_out   = {};
    vm.carros           = {};
    vm.params           = {};
    vm.recoverCounter = 0;
    vm.alertDate = {
        status : false,
        msg : ''
    };

    //Default params debido al no cambio de la API
    vm.carParams = {
        "Currency":"MXN",
        "PLI":1,
        "CDW":1,
        "PAI":0,
        "DP":0,
        "CA":0,
        "CM":0,
        "GPS":0,
        "BS":0,
        "DealId":"0"
    };

    /* Getters */
    vm.getEstaciones    = getEstaciones;
    vm.getCarros        = getCarros;
    vm.verificarDisponibilidad = verificarDisponibilidad;
    vm.mostrarDisponibles = mostrarDisponibles;
    vm.modificarItinerario = modificarItinerario;

    /* Spinners */
    vm.spinner = {
        estaciones_in : true,
        estaciones_out : true,
        carros : false
    };

    /* Show control */
    vm.sections = {
        form : true,
        disp : false
    };

    /* Utilities */
    vm.formatoFecha = formatoFecha;
    vm.intentosRecuperacion = 0;
    vm.sessionRecovery = sessionRecovery;


    activate();

    function activate() {

        /*** Si no existe la session en caché, se procederá a generarla y consultar estaciones ***/
        if(!localStorage.autoClickSession){
            vm.root = angular.element('#rootAutoclick').scope().rt;
            vm.root.generarSession().then(function(){

                vm.getEstaciones('CheckIn', 'estaciones_in').then(function(){
                    vm.getEstaciones('CheckOut', 'estaciones_out');
                });

            });
        }
        /*** Se consulta estaciones con variable en cache ***/
        else{
            vm.getEstaciones('CheckIn', 'estaciones_in').then(function(){
                vm.getEstaciones('CheckOut', 'estaciones_out');
            });

        }

    }

    /*** Obtiene los carros segun el itinerario enviado ***/
    function getCarros(){
        vm.spinner.carros = true;
        vm.formatoCarroParams["SessionId"] = localStorage.autoClickSession;

        return Carro.getAll(vm.formatoCarroParams)
            .then(function(response) {
                vm.mostrarDisponibles();
                vm.detalleItinerario = response.data;
                vm.carros = response.data.CarList;
                vm.spinner.carros = false;
                return vm.carros;
            }).catch(function(response){
                //Se intenta recuperar session una vez
                if(response.status == 401 && !vm.recoverCounter > 0){
                    console.log('Recuperando sesion...');
                    vm.sessionRecovery('vm.getCarros()');
                }

                vm.spinner.carros = false;
            });

    }

    /*** Obtener las estaciones deseadas
     *    TipoCheck colocara en los parametros del request el tipo de estación solicitada
     *    cont es el nombre del objeto de la vm donde se guardaran las estaciones
     * */
    function getEstaciones(tipoCheck, cont){
        vm.spinner[cont] = true;

        vm.params["SessionId"] = localStorage.autoClickSession;
        vm.params["StationType"] = tipoCheck;

        return Estacion.getAll(vm.params)
            .then(function(response) {
                vm[cont] = response.data;
                vm.spinner[cont] = false;
                return cont;
            }).catch(function(response){
                //Se intenta recuperar session una vez
                if(response.status == 401 && !vm.recoverCounter > 0){
                    console.log('Recuperando sesion...');
                    vm.sessionRecovery('vm.getEstaciones()');
                }

                vm.spinner[cont] = false;
            });
    }

    /*** Formatea fechas, limpia parametros para el request y consulta API de Carros ***/
    function verificarDisponibilidad(){

        vm.alertDate.status = false;

        if(!vm.carParams.CheckInDate || !vm.carParams.CheckOutDate || !vm.carParams.CheckInTime || !vm.carParams.CheckInTime)
        {
            vm.alertDate.status = true;
            return false;
        }

        vm.formatoCarroParams = angular.copy(vm.carParams);

        vm.formatoCarroParams.CheckInDate =  formatoFechaAPI(vm.formatoCarroParams.CheckInDate, vm.formatoCarroParams.CheckInTime);
        vm.formatoCarroParams.CheckOutDate =  formatoFechaAPI(vm.formatoCarroParams.CheckOutDate, vm.formatoCarroParams.CheckOutTime);

        delete vm.formatoCarroParams.CheckInTime;
        delete vm.formatoCarroParams.CheckOutTime;

        vm.getCarros();
    }

    /*** Para mostrar carros disponibles ***/
    function mostrarDisponibles(){
        vm.sections.form = false;
        vm.sections.disp = true;
    }

    /** Para regresar a menu de itinerario **/
    function modificarItinerario(){
        vm.sections.form = true;
        vm.sections.disp = false;
    }

    /*** Formate fecha al formato deseado ***/
    function formatoFecha(fecha, actual, formato){
        return moment(fecha, actual).format(formato);
    }

    /*** Formatea fecha a la que solicita el API uniendo fechas y hora ***/
    function formatoFechaAPI(fecha, hora){
        return moment(fecha + ' ' + hora, 'DD/MM/YYYY HH:mm').format('YYYY-MM-DDTHH:mm');
    }

    /*** Funcion de recuperación por error 401, esta función sólo se genera una vez si se recibe un error 401
     *  en las consultas para verificar que no haya caducado o se haya borrado
     * ***/
    function sessionRecovery(fn){
        if(vm.recoverCounter > 0){
            vm.recoverCounter = 0;
            return false;
        }
        else{
            vm.recoverCounter++;
            vm.root = angular.element('#rootAutoclick').scope().rt;
            vm.root.generarSession().then(function(){
                eval(fn);
                return true;
            }).catch(function(response){
                return true;
            });

        }

    }

}