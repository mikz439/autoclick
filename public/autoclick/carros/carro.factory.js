(function(){
    angular
        .module('autoclick')
        .factory('Carro', Carro);

    Carro.$inject = ['$http', '$q'];

    function Carro($http, $q) {

        var urlBase = '/api/cars';

        return {
            getAll: getAll
        };

        function getAll(e) {
            return $http.get(urlBase, {params: e})
                .then(function(response){
                        return response;
                    },
                    function(error){
                        throw error;
                    })
                .catch(function(error){
                    return $q.reject(error);
                });
        };
    }
})();
