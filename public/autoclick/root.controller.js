angular
    .module('autoclick')
    .controller('RootController', RootController);

RootController.$inject = ['Keep'];

function RootController(Keep) {
    var vm = this;

    vm.checarSession = checarSession;
    vm.generarSession = generarSession;

    /*** Genera la SessionId y la guarda en caché para optimización ***/
    function generarSession(){
        return Keep.createSession()
            .then(function(response){
                vm.key = response.data.SessionId;
                localStorage.setItem('autoClickSession', vm.key);
                return vm.key;
            }).catch(function(response){
                vm.alerta = response;
                return response;
            });
    }

    /*** Verifica si existe la sesión, si no existe se regenera ***/
    function checarSession(){
        if(localStorage.autoClickSession){
            return localStorage.autoClickSession;
        }else{
            generarSession();
            return localStorage.autoClickSession;
        }
    }


}