(function(){
    angular
        .module('autoclick')
        .factory('Keep', Keep);

    Keep.$inject = ['$http', '$q'];

    function Keep($http, $q) {

        var urlBase = '/api/generateSession';

        var key = {
            "LanguageId" : "ES",
            "Password" : "0123456789",
            "ContractId" : "0123456789"
        }

        return {
            createSession: createSession
        };

        function createSession(e) {
            return $http.post(urlBase, key)
                .then(function(response){
                        return response;
                    },
                    function(error){
                        throw error;
                    })
                .catch(function(error){
                    return $q.reject(error);
                });
        };

    }
})();
